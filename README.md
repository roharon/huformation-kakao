# HUFormation
> 카카오톡 @훕포메이션 을 통해서 사용할 수 있습니다.

> roharon@hufs.ac.kr

<html>
<p>

<br>
  <h2>개발 환경</h2>
    <ul>
    <li>Ubuntu 16.04</li>
    <li>PyCharm</li>
    <li>Python 3.6</li>
    <li>Django 1.11.2</li>
    </ul>
    
</p>
<p>
<h2>사용된 모듈
</h2>
<ul>
<li>beautifulsoup4</li>
<li>Django</li>
<li>lxml</li>
<li>requests</li>
<li>sqlite3 (Django에 내장</li>
</ul>
자세한 부분은 requirements.txt 참조

</p>
    
    
    
    
</html>
